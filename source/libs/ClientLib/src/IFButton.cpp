#include "StdAfx.h"
#include "IFButton.h"

void CIFButton::sub_653DC0(std::n_wstring *str) {
    reinterpret_cast<void (__thiscall *)(CIFButton *, std::n_wstring *)>(0x653DC0)(this, str);
}

void CIFButton::sub_652D20(int a2) {
    reinterpret_cast<void (__thiscall *)(CIFButton *, int)>(0x652D20)(this, a2);
}
