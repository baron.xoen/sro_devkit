#include "../../../DevKit_DLL/src/StdAfx.h"
#include "IFDiscordGuide.h"
#include "GEffSoundBody.h"
#include "GInterface.h"
#include <Windows.h>
#include <string>
#include <iostream>


GFX_IMPLEMENT_DYNCREATE(CIFDiscordGuide, CIFDecoratedStatic)

bool CIFDiscordGuide::OnCreate(long ln)
{
	printf("> " __FUNCTION__ "(%d)\n", ln);
	CIFDecoratedStatic::OnCreate(ln);

	TB_Func_13("icon\\etc\\discord_1.ddj", 0, 0);
	sub_634470("icon\\etc\\discord_2.ddj");
	
	set_N00009BD4(2);
	set_N00009BD3(500);

	CGEffSoundBody::get()->PlaySound(L"snd_quest");


	return true;
}

int CIFDiscordGuide::OnMouseLeftUp(int a1, int x, int y)
{
	printf("> " __FUNCTION__ "(%d, %d, %d)\n", a1, x, y);

	CGEffSoundBody::get()->PlaySound(L"snd_quest");
		ShellExecute(NULL, "open", "https://discord.gg/R7wJ4ux", NULL, NULL, SW_SHOWNORMAL);
	return 0;
}

void CIFDiscordGuide::OnCIFReady()
{
	printf("> " __FUNCTION__ "\n");

	CIFDecoratedStatic::OnCIFReady();
	sub_633990();

}